# Polish translation for ubuntu-filemanager-app
# Copyright (c) 2013 Rosetta Contributors and Canonical Ltd 2013
# This file is distributed under the same license as the ubuntu-filemanager-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-filemanager-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-07-03 13:35+0000\n"
"PO-Revision-Date: 2019-10-26 17:36+0000\n"
"Last-Translator: Daniel Frańczak <danielfranczak5@wp.pl>\n"
"Language-Team: Polish <https://translate.ubports.com/projects/ubports/"
"filemanager-app/pl/>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.8\n"
"X-Launchpad-Export-Date: 2017-04-08 06:04+0000\n"

#: ../src/app/qml/actions/AddBookmark.qml:6
msgid "Add bookmark"
msgstr "Dodaj zakładkę"

#: ../src/app/qml/actions/ArchiveExtract.qml:6
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:18
#: ../src/app/qml/dialogs/OpenWithDialog.qml:33
msgid "Extract archive"
msgstr "Rozpakuj archiwum"

#: ../src/app/qml/actions/Delete.qml:5
#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:14
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:15
#: ../src/app/qml/panels/SelectionBottomBar.qml:26
msgid "Delete"
msgstr "Usuń"

#: ../src/app/qml/actions/FileClearSelection.qml:8
msgid "Clear clipboard"
msgstr "Wyczyść schowek"

#: ../src/app/qml/actions/FileCopy.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:45
msgid "Copy"
msgstr "Kopiuj"

#: ../src/app/qml/actions/FileCut.qml:5
#: ../src/app/qml/panels/SelectionBottomBar.qml:60
msgid "Cut"
msgstr "Wytnij"

#: ../src/app/qml/actions/FilePaste.qml:30
#, qt-format
msgid "Paste %1 file"
msgid_plural "Paste %1 files"
msgstr[0] "Wklej %1 plik"
msgstr[1] "Wklej %1 pliki"
msgstr[2] "Wklej %1 plików"

#: ../src/app/qml/actions/GoBack.qml:5
msgid "Go back"
msgstr "Wróć"

#: ../src/app/qml/actions/GoNext.qml:5
msgid "Go next"
msgstr "Dalej"

#: ../src/app/qml/actions/GoTo.qml:6
msgid "Go To"
msgstr "Przejdź do"

#: ../src/app/qml/actions/NewItem.qml:6
msgid "New Item"
msgstr "Nowy element"

#: ../src/app/qml/actions/PlacesBookmarks.qml:6
#: ../src/app/qml/ui/PlacesPage.qml:16
msgid "Places"
msgstr "Miejsca"

#: ../src/app/qml/actions/Properties.qml:6
#: ../src/app/qml/dialogs/OpenWithDialog.qml:54
msgid "Properties"
msgstr "Właściwości"

#: ../src/app/qml/actions/Rename.qml:5
#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:20
msgid "Rename"
msgstr "Zmień nazwę"

#: ../src/app/qml/actions/Select.qml:5
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:60
msgid "Select"
msgstr "Wybierz"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
msgid "Select None"
msgstr "Odznacz wszystko"

#: ../src/app/qml/actions/SelectUnselectAll.qml:6
msgid "Select All"
msgstr "Zaznacz wszystko"

#: ../src/app/qml/actions/Settings.qml:6
msgid "Settings"
msgstr "Ustawienia"

#: ../src/app/qml/actions/Share.qml:5
msgid "Share"
msgstr "Udostępnij"

#: ../src/app/qml/actions/TabsAdd.qml:5
msgid "Add tab"
msgstr "Dodaj kartę"

#: ../src/app/qml/actions/TabsCloseThis.qml:5
msgid "Close this tab"
msgstr "Zamknij kartę"

#: ../src/app/qml/actions/TabsOpenInNewTab.qml:5
msgid "Open in a new tab"
msgstr "Otwórz w nowej karcie"

#: ../src/app/qml/actions/UnlockFullAccess.qml:6
msgid "Unlock"
msgstr "Odblokuj"

#: ../src/app/qml/authentication/FingerprintDialog.qml:26
#: ../src/app/qml/authentication/PasswordDialog.qml:43
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:28
msgid "Authentication required"
msgstr "Wymagane jest uwierzytelnienie"

#: ../src/app/qml/authentication/FingerprintDialog.qml:27
msgid "Use your fingerprint to access restricted content"
msgstr "Użyj odcisku palca, aby uzyskać dostęp do zastrzeżonych treści"

#. TRANSLATORS: "Touch" here is a verb
#: ../src/app/qml/authentication/FingerprintDialog.qml:55
msgid "Touch sensor"
msgstr "Czujnik dotyku"

#: ../src/app/qml/authentication/FingerprintDialog.qml:60
msgid "Use password"
msgstr "Użyj hasła"

#: ../src/app/qml/authentication/FingerprintDialog.qml:69
#: ../src/app/qml/authentication/PasswordDialog.qml:80
#: ../src/app/qml/dialogs/CreateItemDialog.qml:60
#: ../src/app/qml/dialogs/ExtractingDialog.qml:35
#: ../src/app/qml/dialogs/FileActionDialog.qml:45
#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:44
#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:96
#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:38
#: ../src/app/qml/dialogs/OpenWithDialog.qml:64
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:41
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:60
#: ../src/app/qml/ui/FileDetailsPopover.qml:184
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:51
#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:26
msgid "Cancel"
msgstr "Anuluj"

#: ../src/app/qml/authentication/FingerprintDialog.qml:126
msgid "Authentication failed!"
msgstr "Uwierzytelnianie nie powiodło się!"

#: ../src/app/qml/authentication/FingerprintDialog.qml:137
msgid "Please retry"
msgstr "Proszę spróbuj ponownie"

#: ../src/app/qml/authentication/PasswordDialog.qml:35
msgid "Authentication failed"
msgstr "Uwierzytelnienie nie powiodło się"

#: ../src/app/qml/authentication/PasswordDialog.qml:45
msgid "Your passphrase is required to access restricted content"
msgstr "Twoje hasło jest wymagane, aby uzyskać dostęp do zastrzeżonych treści"

#: ../src/app/qml/authentication/PasswordDialog.qml:46
msgid "Your passcode is required to access restricted content"
msgstr "Twoje hasło jest wymagane, aby uzyskać dostęp do zastrzeżonych treści"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passphrase (default is 0000 if unset)"
msgstr "hasło (domyślnie 0000 jeśli nie ustawiono)"

#: ../src/app/qml/authentication/PasswordDialog.qml:57
msgid "passcode (default is 0000 if unset)"
msgstr "hasło (domyślnie 0000 jeśli nie ustawiono)"

#: ../src/app/qml/authentication/PasswordDialog.qml:69
msgid "Authentication failed. Please retry"
msgstr "Uwierzytelnianie nie powiodło się. Proszę spróbuj ponownie"

#: ../src/app/qml/authentication/PasswordDialog.qml:74
msgid "Authenticate"
msgstr "Uwierzytelnij"

#: ../src/app/qml/backend/FolderListModel.qml:123
#, qt-format
msgid "%1 file"
msgstr "%1 plik"

#: ../src/app/qml/backend/FolderListModel.qml:124
#, qt-format
msgid "%1 files"
msgstr "%1 pliki"

#: ../src/app/qml/backend/FolderListModel.qml:135
msgid "Folder"
msgstr "Katalog"

#: ../src/app/qml/components/TextualButtonStyle.qml:48
#: ../src/app/qml/components/TextualButtonStyle.qml:56
#: ../src/app/qml/components/TextualButtonStyle.qml:63
msgid "Pick"
msgstr "Wybierz"

#: ../src/app/qml/content-hub/FileOpener.qml:27
msgid "Open with"
msgstr "Otwórz za pomocą"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:15
msgid "Extract Archive"
msgstr "Rozpakuj archiwum"

#: ../src/app/qml/dialogs/ConfirmExtractDialog.qml:16
#, qt-format
msgid "Are you sure you want to extract '%1' here?"
msgstr "Czy na pewno tutaj rozpakować '%1'?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:16
#, qt-format
msgid "Are you sure you want to permanently delete '%1'?"
msgstr "Czy na pewno trwale usunąć '%1'?"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:15
msgid "these files"
msgstr "te pliki"

#: ../src/app/qml/dialogs/ConfirmMultipleDeleteDialog.qml:18
#: ../src/app/qml/dialogs/ConfirmSingleDeleteDialog.qml:21
msgid "Deleting files"
msgstr "Usuwanie plików"

#: ../src/app/qml/dialogs/ConfirmRenameDialog.qml:21
msgid "Enter a new name"
msgstr "Podaj nową nazwę"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:13
msgid "Create Item"
msgstr "Utwórz element"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:14
msgid "Enter name for new item"
msgstr "Wpisz nazwę dla nowego elementu"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:18
msgid "Item name"
msgstr "Nazwa elementu"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:24
msgid "Create file"
msgstr "Utwórz plik"

#: ../src/app/qml/dialogs/CreateItemDialog.qml:43
msgid "Create Folder"
msgstr "Utwórz katalog"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:23
#, qt-format
msgid "Extracting archive '%1'"
msgstr "Rozpakowywanie archiwum '%1'"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:44
#: ../src/app/qml/dialogs/NotifyDialog.qml:27
#: ../src/app/qml/dialogs/templates/ConfirmDialog.qml:31
#: ../src/app/qml/dialogs/templates/ConfirmDialogWithInput.qml:49
msgid "OK"
msgstr "OK"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:59
msgid "Extracting failed"
msgstr "Rozpakowanie nie powiodło się"

#: ../src/app/qml/dialogs/ExtractingDialog.qml:60
#, qt-format
msgid "Extracting the archive '%1' failed."
msgstr "Rozpakowanie archiwum '%1' nie powiodło się."

#: ../src/app/qml/dialogs/FileActionDialog.qml:30
msgid "Choose action"
msgstr "Wybierz akcję"

#: ../src/app/qml/dialogs/FileActionDialog.qml:31
#, qt-format
msgid "For file: %1"
msgstr "Dla pliku: %1"

#: ../src/app/qml/dialogs/FileActionDialog.qml:35
msgid "Open"
msgstr "Otwórz"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:27
msgid "Operation in progress"
msgstr "Operacja w toku"

#: ../src/app/qml/dialogs/FileOperationProgressDialog.qml:29
msgid "File operation"
msgstr "Operacje na pliku"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:45
msgid "User"
msgstr "Użytkownik"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:58
msgid "Password"
msgstr "Hasło"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:72
msgid "Save password"
msgstr "Zapisz hasło"

#: ../src/app/qml/dialogs/NetAuthenticationDialog.qml:108
msgid "Ok"
msgstr "OK"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:8
msgid "Archive file"
msgstr "Plik archiwum"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:9
msgid "Do you want to extract the archive here?"
msgstr "Czy rozpakować to archiwum tutaj?"

#: ../src/app/qml/dialogs/OpenArchiveDialog.qml:28
#: ../src/app/qml/dialogs/OpenWithDialog.qml:44
msgid "Open with another app"
msgstr "Otwórz w innym programie"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:8
msgid "Open file"
msgstr "Otwórz plik"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:9
msgid "What do you want to do with the clicked file?"
msgstr "Co chcesz zrobić z plikiem click?"

#: ../src/app/qml/dialogs/OpenWithDialog.qml:22
msgid "Preview"
msgstr "Podgląd"

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "File %1"
msgstr "Plik %1"

#: ../src/app/qml/filemanager.qml:170
#, qt-format
msgid "%1 Files"
msgstr "%1 Pliki"

#: ../src/app/qml/filemanager.qml:171
#, qt-format
msgid "Saved to: %1"
msgstr "Zapisano: %1"

#: ../src/app/qml/panels/DefaultBottomBar.qml:24
msgid "Paste files"
msgstr "Wklej pliki"

#: ../src/app/qml/ui/FileDetailsPopover.qml:34
msgid "Readable"
msgstr "Do odczytu"

#: ../src/app/qml/ui/FileDetailsPopover.qml:37
msgid "Writable"
msgstr "Do zapisu"

#: ../src/app/qml/ui/FileDetailsPopover.qml:40
msgid "Executable"
msgstr "Wykonywalny"

#: ../src/app/qml/ui/FileDetailsPopover.qml:100
msgid "Where:"
msgstr "Gdzie:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:127
msgid "Created:"
msgstr "Utworzony:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:141
msgid "Modified:"
msgstr "Zmodyfikowano:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:155
msgid "Accessed:"
msgstr "Ostatnio otwarty:"

#: ../src/app/qml/ui/FileDetailsPopover.qml:176
msgid "Permissions:"
msgstr "Uprawnienia:"

#: ../src/app/qml/ui/FolderListPage.qml:241
#: ../src/app/qml/ui/FolderListPage.qml:296
msgid "Restricted access"
msgstr "Ograniczony dostęp"

#: ../src/app/qml/ui/FolderListPage.qml:245
msgid ""
"Authentication is required in order to see all the content of this folder."
msgstr ""
"Uwierzytelnienie jest wymagane, aby zobaczyć całą zawartość tego folderu."

#: ../src/app/qml/ui/FolderListPage.qml:282
msgid "No files"
msgstr "Brak plików"

#: ../src/app/qml/ui/FolderListPage.qml:283
msgid "This folder is empty."
msgstr "Ten folder jest pusty."

#: ../src/app/qml/ui/FolderListPage.qml:297
msgid "Authentication is required in order to see the content of this folder."
msgstr "Uwierzytelnienie jest wymagane, aby zobaczyć zawartość tego folderu."

#: ../src/app/qml/ui/FolderListPage.qml:313
msgid "File operation error"
msgstr "Błąd operacji na pliku"

#: ../src/app/qml/ui/FolderListPageDefaultHeader.qml:21
#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
#, qt-format
msgid "%1 item"
msgid_plural "%1 items"
msgstr[0] "element"
msgstr[1] "elementy"
msgstr[2] "elementów"

#: ../src/app/qml/ui/FolderListPagePickModeHeader.qml:21
msgid "Save here"
msgstr "Zapisz tutaj"

#: ../src/app/qml/ui/FolderListPageSelectionHeader.qml:18
#, qt-format
msgid "%1 item selected"
msgid_plural "%1 items selected"
msgstr[0] "%1 wybrany element"
msgstr[1] "%1 wybrane elementy"
msgstr[2] "%1 wybranych elementów"

#: ../src/app/qml/ui/ViewPopover.qml:24
msgid "Show Hidden Files"
msgstr "Pokaż ukryte pliki"

#: ../src/app/qml/ui/ViewPopover.qml:37
msgid "View As"
msgstr "Wyświetl jako"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "List"
msgstr "Lista"

#: ../src/app/qml/ui/ViewPopover.qml:39
msgid "Icons"
msgstr "Ikony"

#: ../src/app/qml/ui/ViewPopover.qml:44
msgid "Grid size"
msgstr "Wielkość kratki"

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "S"
msgstr "Pd."

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "M"
msgstr "M"

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "L"
msgstr "L"

#: ../src/app/qml/ui/ViewPopover.qml:47 ../src/app/qml/ui/ViewPopover.qml:55
msgid "XL"
msgstr "XL"

#: ../src/app/qml/ui/ViewPopover.qml:52
msgid "List size"
msgstr "Rozmiar listy"

#: ../src/app/qml/ui/ViewPopover.qml:60
msgid "Sort By"
msgstr "Sortuj według"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Name"
msgstr "Etykieta"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Date"
msgstr "Data"

#: ../src/app/qml/ui/ViewPopover.qml:62
msgid "Size"
msgstr "Rozmiar"

#: ../src/app/qml/ui/ViewPopover.qml:67
msgid "Sort Order"
msgstr "Kolejność sortowania"

#: ../src/app/qml/ui/ViewPopover.qml:74
msgid "Theme"
msgstr "Motyw"

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Light"
msgstr "Jasny"

#: ../src/app/qml/ui/ViewPopover.qml:76
msgid "Dark"
msgstr "Ciemny"

#: ../src/app/qml/views/FolderDelegateActions.qml:40
msgid "Folder not accessible"
msgstr "Katalog niedostępny"

#. TRANSLATORS: this refers to a folder name
#: ../src/app/qml/views/FolderDelegateActions.qml:42
#, qt-format
msgid "Can not access %1"
msgstr "Nie można uzyskać dostępu do %1"

#: ../src/app/qml/views/FolderDelegateActions.qml:136
msgid "Could not rename"
msgstr "Nie udało się zmienić nazwy"

#: ../src/app/qml/views/FolderDelegateActions.qml:137
msgid ""
"Insufficient permissions, name contains special chars (e.g. '/'), or already "
"exists"
msgstr ""
"Niewystarczające uprawnienia, nazwa zawiera znaki specjalne (np. \"/\") lub "
"już istnieje"

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Directories"
msgstr "Katalogi"

#: ../src/app/qml/views/FolderListView.qml:72
msgid "Files"
msgstr "Pliki"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:360
msgid "Unknown"
msgstr "Nieznane"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:487
msgid "path or url may not exist or cannot be read"
msgstr "Ścieżka lub adres URL nie istnieje lub nie mogą zostać odczytane"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:690
msgid "Rename error"
msgstr "Błąd zmiany nazwy"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:713
msgid "Error creating new folder"
msgstr "Błąd podczas tworzenia nowego katalogu"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:740
msgid "Touch file error"
msgstr "Błąd pliku Touch"

#: ../src/plugin/folderlistmodel/dirmodel.cpp:1353
msgid "items"
msgstr "elementy/ów"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:320
msgid "File or Directory does not exist"
msgstr "Plik lub katalog nie istnieje"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:321
msgid " does not exist"
msgstr " nie istnieje"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:326
msgid "Cannot access File or Directory"
msgstr "Brak dostępu do pliku lub katalogu"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:327
msgid " it needs Authentication"
msgstr " akcja wymaga uwierzytelnienia"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:361
msgid "Cannot copy/move items"
msgstr "Nie można skopiować/przenieść plików"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:362
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1487
msgid "no write permission on folder "
msgstr "Brak uprawnień do zapisu w katalogu "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:642
msgid "Could not remove the item "
msgstr "nie można usunąć "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:680
msgid "Could not find a suitable name to backup"
msgstr "Nie udało się znaleźć odpowiedniej nazwy dla kopii"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:733
msgid "Could not create the directory"
msgstr "Nie można utworzyć katalogu"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:738
msgid "Could not create link to"
msgstr "Nie można utworzyć linku do"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:746
msgid "Could not set permissions to dir"
msgstr "Nie można zmienić uprawnień katalogu"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:756
msgid "Could not open file"
msgstr "Nie można otworzyć pliku"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:772
msgid "There is no space to copy"
msgstr "Brak miejsca na skopiowanie pliku"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:780
msgid "Could not create file"
msgstr "Nie można utworzyć pliku"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:837
msgid "Could not remove the directory/file "
msgstr "Nie można usunąć pliku/katalogu "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:850
msgid "Could not move the directory/file "
msgstr "Nie można przenieść pliku/katalogu "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1089
msgid "Write error in "
msgstr "Błąd zapisu w "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1101
msgid "Read error in "
msgstr "Błąd odczytu w "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1318
msgid " Copy"
msgstr " Kopiuj"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1371
msgid "Set permissions error in "
msgstr "Błąd zmiany uprawnień dla "

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1437
msgid "Could not create trash info file"
msgstr "Nie można utworzyć pliku trash info"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1449
msgid "Could not remove the trash info file"
msgstr "Nie można usunąć pliku trash info"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1479
#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1486
msgid "Cannot move items"
msgstr "Nie można przenieść elementów"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1481
msgid "origin and destination folders are the same"
msgstr "Folder źródłowy i docelowy są takie same"

#: ../src/plugin/folderlistmodel/filesystemaction.cpp:1524
msgid "There is no space to download"
msgstr "Brak miejsca aby pobrać plik"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:72
msgid "net tool not found, check samba installation"
msgstr "nie znaleziono narzędzia sieciowego, sprawdź instalację samby"

#: ../src/plugin/folderlistmodel/smb/qsambaclient/src/smbusershare.cpp:80
msgid "cannot write in "
msgstr "nie można zapisać w "

#: com.ubuntu.filemanager.desktop.in.in.h:1
msgid "File Manager"
msgstr "Menedżer plików"

#: com.ubuntu.filemanager.desktop.in.in.h:2
msgid "folder;manager;explore;disk;filesystem;"
msgstr "folder;manager;explore;dysk;system plików;"

#~ msgid "About Application"
#~ msgstr "O programie"

#~ msgid ""
#~ "The <b>Application</b> example demonstrates how to write modern GUI "
#~ "applications using Qt, with a menu bar, toolbars, and a status bar."
#~ msgstr ""
#~ "Przykład <b>Aplikacji</b> ilustruje sposób pisania nowoczesnych aplikacji "
#~ "GUI przy użyciu protokołu Qt, z paskiem menu, paskami narzędzi i paskiem "
#~ "stanu."

#~ msgid "&New"
#~ msgstr "&Nowy"

#~ msgid "Create a new file"
#~ msgstr "Tworzy nowy plik"

#~ msgid "&Open..."
#~ msgstr "&Otwórz..."

#~ msgid "Open an existing file"
#~ msgstr "Otwiera istniejący plik"

#~ msgid "&Save"
#~ msgstr "&Zapisz"

#~ msgid "Save the document to disk"
#~ msgstr "Zapisuje plik na dysku"

#~ msgid "Save &As..."
#~ msgstr "Z&apisz Jako..."

#~ msgid "Save the document under a new name"
#~ msgstr "Zapisuje plik pod nową nazwą"

#~ msgid "E&xit"
#~ msgstr "&Wyjście"

#~ msgid "Exit the application"
#~ msgstr "Zamyka program"

#~ msgid "Cu&t"
#~ msgstr "Wy&tnij"

#~ msgid "Cut the current selection's contents to the clipboard"
#~ msgstr "Wycina obecne zaznaczenie, zapisując zawartość do schowka"

#~ msgid "&Copy"
#~ msgstr "&Kopiuj"

#~ msgid "Copy the current selection's contents to the clipboard"
#~ msgstr "Kopiuje zaznaczenie do schowka"

#~ msgid "&Paste"
#~ msgstr "Wkle&j"

#~ msgid "Paste the clipboard's contents into the current selection"
#~ msgstr "Wkleja zawartość schowka do obecnego zaznaczenia"

#~ msgid "&About"
#~ msgstr "&O programie"

#~ msgid "Show the application's About box"
#~ msgstr "Pokazuje informacje o programie"

#~ msgid "About &Qt"
#~ msgstr "O &Qt"

#~ msgid "Show the Qt library's About box"
#~ msgstr "Pokazuje informacje o bibliotece Qt"

#~ msgid "&File"
#~ msgstr "&Plik"

#~ msgid "&Edit"
#~ msgstr "&Edytuj"

#~ msgid "&Help"
#~ msgstr "Po&moc"

#~ msgid "Edit"
#~ msgstr "Edytuj"

#~ msgid "Ready"
#~ msgstr "Gotowy"

#~ msgid "Application"
#~ msgstr "Program"

#~ msgid ""
#~ "The document has been modified.\n"
#~ "Do you want to save your changes?"
#~ msgstr ""
#~ "Plik został zmodyfikowany.\n"
#~ "Czy chcesz zapisać zmiany?"

#~ msgid ""
#~ "Cannot read file %1:\n"
#~ "%2."
#~ msgstr ""
#~ "Nie można odczytać pliku %1:\n"
#~ "%2."

#~ msgid "File loaded"
#~ msgstr "Wczytano plik"

#~ msgid ""
#~ "Cannot write file %1:\n"
#~ "%2."
#~ msgstr ""
#~ "Nie można zapisać pliku %1:\n"
#~ "%2."

#~ msgid "File saved"
#~ msgstr "Zapisano plik"

#~ msgid "%1 (%2 file)"
#~ msgid_plural "%1 (%2 files)"
#~ msgstr[0] "%1 (%2 plik)"
#~ msgstr[1] "%1 (%2 pliki)"
#~ msgstr[2] "%1 (%2 plików)"

#~ msgid "Device"
#~ msgstr "Urządzenie"

#~ msgid "Change app settings"
#~ msgstr "Zmień ustawienia aplikacji"

#~ msgid "password"
#~ msgstr "Hasło"

#~ msgid "Path:"
#~ msgstr "Ścieżka:"

#~ msgid "Contents:"
#~ msgstr "Zawartość:"

#~ msgid "Unlock full access"
#~ msgstr "Odblokuj pełny dostęp"

#~ msgid "Enter name for new folder"
#~ msgstr "Podaj nazwę dla nowego katalogu"

#~ msgid "~/Desktop"
#~ msgstr "~/Pulpit"

#~ msgid "~/Public"
#~ msgstr "~/Publiczne"

#~ msgid "~/Programs"
#~ msgstr "~/Programy"

#~ msgid "~/Templates"
#~ msgstr "~/Szablony"

#~ msgid "Home"
#~ msgstr "Katalog domowy"

#~ msgid "Network"
#~ msgstr "Sieć"

#~ msgid "Go To Location"
#~ msgstr "Przejdź do lokalizacji"

#~ msgid "Enter a location to go to:"
#~ msgstr "Wprowadź lokalizację, aby przejść do:"

#~ msgid "Location..."
#~ msgstr "Lokalizacja..."

#~ msgid "Go"
#~ msgstr "Idź"

#~ msgid "Show Advanced Features"
#~ msgstr "Pokaż zaawansowane funkcje"

#~ msgid "Ascending"
#~ msgstr "Rosnąco"

#~ msgid "Descending"
#~ msgstr "Malejąco"

#~ msgid "Filter"
#~ msgstr "Filtrowanie"
